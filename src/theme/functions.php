<?php

/** analytics */
// require(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require(get_stylesheet_directory().'/includes/store-changes.php');

add_action('wp_enqueue_scripts', 'eaf_resources');
function eaf_resources(){
	etheme_child_styles();
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'eaf_theme_lang' );
function eaf_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}