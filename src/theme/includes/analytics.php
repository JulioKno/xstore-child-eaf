<?php
//Google analytics tag
add_action( 'wp_head', 'nap_google_analytics', 7 );
function nap_google_analytics() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-XXXXXXXXX-X"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-XXXXXXXXX-X');
    </script>
    <?php
}

/**
 * Funciones avanzadas de google analytics para ecommerce
 * 
 */

// //Transacción completa
// function nap_ga_add_transaction($order_id){
//   $order = wc_get_order( $order_id );

//   if ( ! $order ) {
//     return;
//   }
//   $ga_code = "ga( 'set', 'currencyCode', '" . esc_js( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0', '<' ) ? $order->get_order_currency() : $order->get_currency() ) . "' );";


//   if ( $order->get_items() ) {
//     foreach ( $order->get_items() as $item ) {
//       $ga_code .= nap_ga_add_item( $order, $item );
//     }
//   }

//   $coupons = $order->get_used_coupons() ? implode(',', $order->get_used_coupons()) : 0;

//   $ga_code .= "ga( 'ec:setAction', 'purchase', {
//     'id': '" . esc_js( $order->get_order_number() ) . "',
//     'affiliation': '" . esc_js( get_bloginfo( 'name' ) ) . "',
//     'revenue': '" . esc_js( $order->get_total() ) . "',
//     'tax': '" . esc_js( $order->get_total_tax() ) . "',
//     'shipping': '" . esc_js( $order->get_total_shipping() ) . "'". 
//     ($coupons ? ",'coupon': '".esc_js($coupons)."'" : "")."
//   } );";

//   // Mark the order as tracked.
//   update_post_meta( $order_id, '_ga_tracked', 1 );
//   return "<script type='text/javascript'>".$ga_code."</script>";
// }

// function nap_ga_add_item($order, $item){
//   $_product = defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0', '<' ) ? $order->get_product_from_item( $item ) : $item->get_product();
//   $variant  = nap_ga_get_variant_line( $_product );

//   $item_code = "ga( 'ec:addProduct', {";
//   $item_code .= "'id': '" . esc_js( $_product->get_sku() ? $_product->get_sku() : ( '#' . $_product->get_id() ) ) . "',";
//   $item_code .= "'name': '" . esc_js( $item['name'] ) . "',";
//   $item_code .= "'category': '" . esc_js( $_product->get_attribute('pa_categoria') ) . "',";

//   if ( '' !== $variant ) {
//     $item_code .= "'variant': " . $variant;
//   }

//   $item_code .= "'price': '" . esc_js( $order->get_item_total( $item ) ) . "',";
//   $item_code .= "'quantity': '" . esc_js( $item['qty'] ) . "'";
//   $item_code .= "});";

//   return $item_code;
// }

// function nap_ga_get_variant_line( $_product ) {
//   $out            = '';
//   $variation_data = defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0', '<' ) ? $_product->variation_data : ( $_product->is_type( 'variation' ) ? wc_get_product_variation_attributes( $_product->get_id() ) : '' );

//   if ( is_array( $variation_data ) && ! empty( $variation_data ) ) {
//     $out = "'" . esc_js( wc_get_formatted_variation( $variation_data, true ) ) . "',";
//   }

//   return $out;
// }

// //Agregar al carrito
// add_action( 'woocommerce_after_add_to_cart_button', 'nap_ga_add_to_cart');
// function nap_ga_add_to_cart(){
//   global $product;
//   $ga_code = "ga( 'ec:addProduct', {";
//   $ga_code .= "'id': '" . esc_js( $product->get_sku() ? $product->get_sku() : ( '#' . $product->get_id() ) ) . "',";
//   $ga_code .= "'name': '" . esc_js( $product->get_title() ) . "',";
//   $ga_code .= "'quantity': $( 'input.qty' ).val() ? $( 'input.qty' ).val() : '1'";
//   $ga_code .= "} );";

//   $selector = '.single_add_to_cart_button';
//   nap_ga_set_add_to_cart_code($selector, $ga_code);
// }


// add_action( 'wp_footer', 'nap_ga_add_to_cart_loop' );
// function nap_ga_add_to_cart_loop(){
//   $ga_code = "ga( 'ec:addProduct', {";
//   $ga_code .= "'id': ($(this).data('product_sku')) ? ($(this).data('product_sku')) : ('#' + $(this).data('product_id')),";
//   $ga_code .= "'quantity': $(this).data('quantity')";
//   $ga_code .= "} );";
  
//   $selector = '.add_to_cart_button:not(.product_type_variable, .product_type_grouped)';
//   nap_ga_set_add_to_cart_code($selector, $ga_code);

// }

// function nap_ga_set_add_to_cart_code($selector, $ga_code){
//   wc_enqueue_js( "
//     $( '" . $selector . "' ).click( function() {
//       " . $ga_code . "
//       ga( 'ec:setAction', 'add' );
//       ga( 'send', 'event', 'UX', 'click', 'add to cart' );
//     });
//   " );
// }


// //Quitar del carrito
// add_action( 'woocommerce_after_cart', 'nap_ga_remove_from_cart' );
// add_action( 'woocommerce_after_mini_cart', 'nap_ga_remove_from_cart' );
// function nap_ga_remove_from_cart(){
//   echo( "
//     <script>
//     (function($) {
//       $( document.body ).on( 'click', '.remove', function() {
//         ga( 'ec:addProduct', {
//           'id': ($(this).data('product_sku')) ? ($(this).data('product_sku')) : ('#' + $(this).data('product_id')),
//           'quantity': $(this).parent().parent().find( '.qty' ).val() ? $(this).parent().parent().find( '.qty' ).val() : '1',
//         } );
//         ga( 'ec:setAction', 'remove' );
//         ga( 'send', 'event', 'UX', 'click', 'remove from cart' );
//       });
//     })(jQuery);
//     </script>
//   " );
// }

// //Vista de detalle de producto
// add_action( 'woocommerce_after_single_product', 'nap_ga_product_detail' );
// function nap_ga_product_detail(){
//   global $product;
//   if ( empty( $product ) ) {
//     return;
//   }

//   wc_enqueue_js( "
//     ga( 'ec:addProduct', {
//       'id': '" . esc_js( $product->get_sku() ? $product->get_sku() : ( '#' . $product->get_id() ) ) . "',
//       'name': '" . esc_js( $product->get_title() ) . "',
//       'category': '" . esc_js($product->get_attribute('pa_categoria')) . "',
//       'price': '" . esc_js( $product->get_price() ) . "',
//     } );

//     ga( 'ec:setAction', 'detail' );" );
// }


// //Set send pageview
// add_action( 'wp_footer', 'nap_ga_send_pageview');
// function nap_ga_send_pageview(){
//   wc_enqueue_js( "ga( 'send', 'pageview' ); " );
// }