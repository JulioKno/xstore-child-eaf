<?php

//mensaje extra en recuperar password
add_action('woocommerce_after_lost_password_confirmation_message', 'eaf_after_lost_password_confirmation_message');
function eaf_after_lost_password_confirmation_message(){
	echo "<p><strong>Si no lo encuentras en la bandeja de entrada, busca en tu bandeja de spam o correo no deseado.</strong></p>";
}